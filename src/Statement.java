public abstract class Statement {
  abstract void eval(Environment env);

  public void evalWithTrigger(Environment env) {
    eval(env);
    if (env.listener != null) {
      env.listener.onPostEval();
    }
  }
}
