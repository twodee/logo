public class StatementPrint extends Statement {
  private Expression expr;

  public StatementPrint(Expression expr) {
    this.expr = expr;
  } 

  public void eval(Environment env) {
    System.out.println(expr.eval(env));
  }
}
