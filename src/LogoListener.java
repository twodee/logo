// Generated from Logo.g by ANTLR 4.5.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LogoParser}.
 */
public interface LogoListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link LogoParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(LogoParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogoParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(LogoParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link LogoParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(LogoParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link LogoParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(LogoParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Repeat}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterRepeat(LogoParser.RepeatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Repeat}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitRepeat(LogoParser.RepeatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Print}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterPrint(LogoParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Print}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitPrint(LogoParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by the {@code If}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIf(LogoParser.IfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code If}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIf(LogoParser.IfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Move}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterMove(LogoParser.MoveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Move}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitMove(LogoParser.MoveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Turn}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterTurn(LogoParser.TurnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Turn}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitTurn(LogoParser.TurnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Assignment}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(LogoParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Assignment}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(LogoParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FunctionDefinition}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(LogoParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionDefinition}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(LogoParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(LogoParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FunctionCall}
	 * labeled alternative in {@link LogoParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(LogoParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Integer}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInteger(LogoParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Integer}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInteger(LogoParser.IntegerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Multiplicative}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative(LogoParser.MultiplicativeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Multiplicative}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative(LogoParser.MultiplicativeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Additive}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAdditive(LogoParser.AdditiveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Additive}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAdditive(LogoParser.AdditiveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Identifier}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(LogoParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Identifier}
	 * labeled alternative in {@link LogoParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(LogoParser.IdentifierContext ctx);
}