public class StatementMove extends Statement {
  private Expression expr;

  public StatementMove(Expression expr) {
    this.expr = expr;
  } 

  public void eval(Environment env) {
    int length = expr.eval(env);
    env.move(length);
  }
}
