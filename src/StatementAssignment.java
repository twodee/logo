public class StatementAssignment extends Statement {
  private String id;
  private Expression expr;

  public StatementAssignment(String id, Expression expr) {
    this.id = id;
    this.expr = expr;
  } 

  public void eval(Environment env) {
    int value = expr.eval(env);
    env.set(id, value);
  }
}
