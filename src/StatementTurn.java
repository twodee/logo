public class StatementTurn extends Statement {
  private Expression expr;

  public StatementTurn(Expression expr) {
    this.expr = expr;
  } 

  public void eval(Environment env) {
    int angle = expr.eval(env);
    env.turn(angle);
  }
}
