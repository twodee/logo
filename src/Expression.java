public interface Expression {
  int eval(Environment env); 
}
