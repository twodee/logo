import java.util.ArrayList;

public class StatementFunctionDefinition extends Statement {
  private String id;
  private Block block;
  private ArrayList<String> formals;
  Environment env;

  public StatementFunctionDefinition(String id, Block block, ArrayList<String> formals) {
    this.id = id;
    this.block = block;
    this.formals = formals;
  } 

  public void eval(Environment env) {
    this.env = env;
    env.setFunction(id, this);
  }

  public ArrayList<String> getFormals() {
    return formals;
  }

  public Block getBlock() {
    return block;
  }
}
