import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map.Entry;

public class StatementFunctionCall extends Statement {
  private String id;
  private ArrayList<Expression> actuals;

  public StatementFunctionCall(String id, ArrayList<Expression> actuals) {
    this.id = id;
    this.actuals = actuals;
  } 

  public void eval(Environment env) {
    StatementFunctionDefinition f = env.getFunction(id);
    
    HashMap<String, Integer> outerScope = f.env.variables;

    // Make outer variables visible
    HashMap<String, Integer> innerScope = (HashMap<String, Integer>) outerScope.clone(); // new HashMap<String, Integer>();

    ArrayList<String> formals = f.getFormals();
    // assert actuals.length == formals.length
 
    for (int i = 0; i < actuals.size(); ++i) {
      innerScope.put(formals.get(i), actuals.get(i).eval(env));
    }

    f.env.variables = innerScope;
    f.getBlock().eval(env);
    f.env.variables = outerScope;

    for (Entry<String, Integer> entry : innerScope.entrySet()) {
      if (outerScope.containsKey(entry.getKey())) {
        outerScope.put(entry.getKey(), entry.getValue());
      }
    }
  }
}
