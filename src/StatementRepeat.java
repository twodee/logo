public class StatementRepeat extends Statement {
  private Expression expr;
  private Block block;

  public StatementRepeat(Expression expr, Block block) {
    this.expr = expr;
    this.block = block;
  } 

  public void eval(Environment env) {
    int n = expr.eval(env);
    for (int i = 0; i < n; ++i) {
      block.eval(env);
    }
  }
}
