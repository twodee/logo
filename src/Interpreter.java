import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import javax.swing.JFileChooser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class Interpreter extends LogoBaseListener {
  public static void main(String[] args) throws IOException {
    JFileChooser chooser = new JFileChooser();
    int status = chooser.showOpenDialog(null);

    if (status == JFileChooser.APPROVE_OPTION) {
      ANTLRInputStream ais = new ANTLRInputStream(new FileInputStream(chooser.getSelectedFile()));
      LogoLexer lexer = new LogoLexer(ais);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      LogoParser parser = new LogoParser(tokens);

      ParseTree tree = parser.program();

      ParseTreeWalker walker = new ParseTreeWalker();
      Interpreter interpreter = new Interpreter();
      walker.walk(interpreter, tree);

      Block program = interpreter.blocks.pop();
      new LogoVirtualMachine(program);

      /* Environment env = new Environment(); */
      /* program.eval(env); */
    }
  }

  Stack<Block> blocks = new Stack<Block>();
  Stack<Expression> exprs = new Stack<Expression>();

  public void enterBlock(LogoParser.BlockContext context) {
    blocks.push(new Block());
  }

  public void exitInteger(LogoParser.IntegerContext context) {
    exprs.push(new ExpressionInteger(Integer.parseInt(context.INTEGER().getText())));
  }

  public void exitIdentifier(LogoParser.IdentifierContext context) {
    exprs.push(new ExpressionIdentifier(context.IDENTIFIER().getText()));
  }

  public void exitMultiplicative(LogoParser.MultiplicativeContext context) {
    Expression b = exprs.pop();
    Expression a = exprs.pop();
    String op = context.op.getText();
    if (op.equals("*")) {
      exprs.push(new ExpressionMultiply(a, b));
    } else if (op.equals("/")) {
      exprs.push(new ExpressionDivide(a, b));
    } else {
      exprs.push(new ExpressionMod(a, b));
    }
  }

  public void exitAdditive(LogoParser.AdditiveContext context) {
    Expression b = exprs.pop();
    Expression a = exprs.pop();
    String op = context.op.getText();
    if (op.equals("+")) {
      exprs.push(new ExpressionPlus(a, b));
    } else {
      exprs.push(new ExpressionMinus(a, b));
    }
  }

  public void exitPrint(LogoParser.PrintContext context) {
    Expression parameter = exprs.pop();
    Statement statement = new StatementPrint(parameter);
    blocks.peek().add(statement);
  }

  public void exitIf(LogoParser.IfContext context) {
    Expression parameter = exprs.pop();
    Block elseBlock = blocks.pop();
    Block thenBlock = blocks.pop();
    Statement statement = new StatementIf(parameter, thenBlock, elseBlock);
    blocks.peek().add(statement);
  }

  public void exitRepeat(LogoParser.RepeatContext context) {
    Expression parameter = exprs.pop();
    Block block = blocks.pop();
    Statement statement = new StatementRepeat(parameter, block);
    blocks.peek().add(statement);
  }

  public void exitMove(LogoParser.MoveContext context) {
    Expression parameter = exprs.pop();
    Statement statement = new StatementMove(parameter);
    blocks.peek().add(statement);
  }

  public void exitTurn(LogoParser.TurnContext context) {
    Expression parameter = exprs.pop();
    Statement statement = new StatementTurn(parameter);
    blocks.peek().add(statement);
  }

  public void exitAssignment(LogoParser.AssignmentContext context) {
    Expression parameter = exprs.pop();
    Statement statement = new StatementAssignment(context.IDENTIFIER().getText(), parameter);
    blocks.peek().add(statement);
  }

  public void exitFunctionDefinition(LogoParser.FunctionDefinitionContext context) {
    ArrayList<String> formals = new ArrayList<String>();
    for (int i = 1; i < context.IDENTIFIER().size(); ++i) {
      String formal = context.IDENTIFIER(i).getText();
      formals.add(formal);
    }

    Block block = blocks.pop();
    Statement statement = new StatementFunctionDefinition(context.IDENTIFIER(0).getText(), block, formals);
    blocks.peek().add(statement);
  }

  public void exitFunctionCall(LogoParser.FunctionCallContext context) {
    ArrayList<Expression> actuals = new ArrayList<Expression>();
    for (int i = 0; i < context.expression().size(); ++i) {
      Expression e = exprs.pop();
      actuals.add(0, e);
    }

    Statement statement = new StatementFunctionCall(context.IDENTIFIER().getText(), actuals);
    blocks.peek().add(statement);
  }
}
