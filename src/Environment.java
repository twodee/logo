import java.util.ArrayList;
import java.awt.geom.Line2D;
import java.util.HashMap;

public class Environment {
  public double x;
  public double y;
  public int heading;
  HashMap<String, Integer> variables;
  private HashMap<String, StatementFunctionDefinition> functions;
  StatementListener listener;
  public ArrayList<Line2D.Double> segments;

  public Environment() {
    variables = new HashMap<String, Integer>();
    functions = new HashMap<String, StatementFunctionDefinition>();
    segments = new ArrayList<Line2D.Double>();
    x = 0.0;
    y = 0.0;
    heading = 0;
    System.err.printf("%f,%f%n", x, y);
  }

  public void move(int length) {
    double oldX = x;
    double oldY = y;
    x += length * Math.cos(Math.toRadians(heading));
    y += length * Math.sin(Math.toRadians(heading));
    synchronized (segments) {
      segments.add(new Line2D.Double(oldX, oldY, x, y));
    }
    System.err.printf("%f,%f%n", x, y);
  }

  public void turn(int angle) {
    heading += angle;
  }

  public int get(String id) {
    if (!variables.containsKey(id)) {
      throw new RuntimeException("No such variable " + id + "!");
    }
    return variables.get(id);
  }

  public void set(String id, int value) {
    variables.put(id, value);
  }

  public StatementFunctionDefinition getFunction(String id) {
    return functions.get(id);
  }

  public void setFunction(String id, StatementFunctionDefinition f) {
    functions.put(id, f);
  }
}
