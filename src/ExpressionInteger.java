public class ExpressionInteger implements Expression {
  private int n;

  public ExpressionInteger(int n) {
    this.n = n; 
  }

  public int eval(Environment env) {
    return n; 
  }
}
