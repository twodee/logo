public class ExpressionMinus implements Expression {
  private Expression a;
  private Expression b;

  public ExpressionMinus(Expression a,
                         Expression b) {
    this.a = a;
    this.b = b;
  }

  public int eval(Environment env) {
    return a.eval(env) - b.eval(env);
  } 
}
