public class ExpressionIdentifier implements Expression {
  private String n;

  public ExpressionIdentifier(String n) {
    this.n = n; 
  }

  public int eval(Environment env) {
    return env.get(n); 
  }
}
