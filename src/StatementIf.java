public class StatementIf extends Statement {
  private Expression expr;
  private Block thenBlock;
  private Block elseBlock;

  public StatementIf(Expression expr, Block thenBlock, Block elseBlock) {
    this.expr = expr;
    this.thenBlock = thenBlock;
    this.elseBlock = elseBlock;
  } 

  public void eval(Environment env) {
    int n = expr.eval(env);
    if (n != 0) {
      thenBlock.eval(env);
    } else {
      elseBlock.eval(env);
    }
  }
}
