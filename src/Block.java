import java.util.ArrayList;

public class Block {
  private ArrayList<Statement> statements;

  public Block() {
    statements = new ArrayList<Statement>();
  }

  public void add(Statement s) {
    statements.add(s);
  }

  public void eval(Environment env) {
    for (Statement s : statements) {
      s.evalWithTrigger(env);
    }
  }
}
