import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class LogoVirtualMachine implements StatementListener {
  private JFrame display;
  private LogoDisplay panel;
  private int delay = 100;

  public LogoVirtualMachine(final Block main) {
    display = new JFrame("Logo");
    panel = new LogoDisplay();
    JScrollPane scroller = new JScrollPane(panel);
    display.add(scroller);

    JButton runButton = new JButton("Run");
    runButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Environment env = new Environment();
        panel.setEnvironment(env);
        env.listener = LogoVirtualMachine.this;
        main.eval(env);
      }
    });
    display.add(runButton, BorderLayout.NORTH);

    panel.setPreferredSize(new Dimension(3000, 3000));
    panel.setSize(panel.getPreferredSize());

    display.setSize(1200, 700);
    display.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    display.setVisible(true);
  }

  public void onPostEval() {
    pause();
    panel.paintImmediately(0, 0, panel.getWidth(), panel.getHeight());
  }

  private void pause() {
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
    }
  }

  class LogoDisplay extends JPanel {
    private Line2D.Double arrowTop;
    private Line2D.Double arrowBottom;
    private Environment env;

    public LogoDisplay() {
      arrowTop = new Line2D.Double(-10.0f, -5.0f, 0.0f, 0.0f);
      arrowBottom = new Line2D.Double(-10.0f, 5.0f, 0.0f, 0.0f);
    }

    public void setEnvironment(Environment env) {
      this.env = env;
    }

    public void paintComponent(Graphics g) {
      super.paintComponent(g);

      if (env == null) {
        return;
      }

      Graphics2D g2 = (Graphics2D) g;
      AffineTransform xform = g2.getTransform();

      g2.translate(1200 / 2, 700 / 2);

      synchronized (env.segments) {
        for (Line2D.Double segment : env.segments) {
          g2.draw(segment);
        }
      }

      g2.translate(env.x, env.y);
      g2.rotate(Math.toRadians(env.heading));
      g2.draw(arrowTop);
      g2.draw(arrowBottom);

      g2.setTransform(xform);
    }
  }
}
