// Generated from Logo.g by ANTLR 4.5.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LogoLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		IF=1, ELSE=2, TO=3, MOVE=4, TURN=5, REPEAT=6, END=7, PRINT=8, LEFT_PARENTHESIS=9, 
		RIGHT_PARENTHESIS=10, MULTIPLY=11, DIVIDE=12, MOD=13, PLUS=14, COMMA=15, 
		MINUS=16, ASSIGNMENT=17, IDENTIFIER=18, INTEGER=19, NEWLINE=20, WHITESPACE=21;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"IF", "ELSE", "TO", "MOVE", "TURN", "REPEAT", "END", "PRINT", "LEFT_PARENTHESIS", 
		"RIGHT_PARENTHESIS", "MULTIPLY", "DIVIDE", "MOD", "PLUS", "COMMA", "MINUS", 
		"ASSIGNMENT", "IDENTIFIER", "INTEGER", "NEWLINE", "WHITESPACE"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'if'", "'else'", "'to'", "'move'", "'turn'", "'repeat'", "'end'", 
		"'print'", "'('", "')'", "'*'", "'/'", "'%'", "'+'", "','", "'-'", "'='", 
		null, null, "'\n'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "IF", "ELSE", "TO", "MOVE", "TURN", "REPEAT", "END", "PRINT", "LEFT_PARENTHESIS", 
		"RIGHT_PARENTHESIS", "MULTIPLY", "DIVIDE", "MOD", "PLUS", "COMMA", "MINUS", 
		"ASSIGNMENT", "IDENTIFIER", "INTEGER", "NEWLINE", "WHITESPACE"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public LogoLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Logo.g"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\27\u0081\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\13"+
		"\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22"+
		"\3\23\6\23g\n\23\r\23\16\23h\3\23\7\23l\n\23\f\23\16\23o\13\23\3\24\5"+
		"\24r\n\24\3\24\6\24u\n\24\r\24\16\24v\3\25\3\25\3\26\6\26|\n\26\r\26\16"+
		"\26}\3\26\3\26\2\2\27\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27"+
		"\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27\3\2\6\4\2C\\c|\5\2"+
		"C\\aac|\3\2\62;\4\2\13\13\"\"\u0085\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2"+
		"\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23"+
		"\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2"+
		"\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2"+
		"\2\2\2+\3\2\2\2\3-\3\2\2\2\5\60\3\2\2\2\7\65\3\2\2\2\t8\3\2\2\2\13=\3"+
		"\2\2\2\rB\3\2\2\2\17I\3\2\2\2\21M\3\2\2\2\23S\3\2\2\2\25U\3\2\2\2\27W"+
		"\3\2\2\2\31Y\3\2\2\2\33[\3\2\2\2\35]\3\2\2\2\37_\3\2\2\2!a\3\2\2\2#c\3"+
		"\2\2\2%f\3\2\2\2\'q\3\2\2\2)x\3\2\2\2+{\3\2\2\2-.\7k\2\2./\7h\2\2/\4\3"+
		"\2\2\2\60\61\7g\2\2\61\62\7n\2\2\62\63\7u\2\2\63\64\7g\2\2\64\6\3\2\2"+
		"\2\65\66\7v\2\2\66\67\7q\2\2\67\b\3\2\2\289\7o\2\29:\7q\2\2:;\7x\2\2;"+
		"<\7g\2\2<\n\3\2\2\2=>\7v\2\2>?\7w\2\2?@\7t\2\2@A\7p\2\2A\f\3\2\2\2BC\7"+
		"t\2\2CD\7g\2\2DE\7r\2\2EF\7g\2\2FG\7c\2\2GH\7v\2\2H\16\3\2\2\2IJ\7g\2"+
		"\2JK\7p\2\2KL\7f\2\2L\20\3\2\2\2MN\7r\2\2NO\7t\2\2OP\7k\2\2PQ\7p\2\2Q"+
		"R\7v\2\2R\22\3\2\2\2ST\7*\2\2T\24\3\2\2\2UV\7+\2\2V\26\3\2\2\2WX\7,\2"+
		"\2X\30\3\2\2\2YZ\7\61\2\2Z\32\3\2\2\2[\\\7\'\2\2\\\34\3\2\2\2]^\7-\2\2"+
		"^\36\3\2\2\2_`\7.\2\2` \3\2\2\2ab\7/\2\2b\"\3\2\2\2cd\7?\2\2d$\3\2\2\2"+
		"eg\t\2\2\2fe\3\2\2\2gh\3\2\2\2hf\3\2\2\2hi\3\2\2\2im\3\2\2\2jl\t\3\2\2"+
		"kj\3\2\2\2lo\3\2\2\2mk\3\2\2\2mn\3\2\2\2n&\3\2\2\2om\3\2\2\2pr\7/\2\2"+
		"qp\3\2\2\2qr\3\2\2\2rt\3\2\2\2su\t\4\2\2ts\3\2\2\2uv\3\2\2\2vt\3\2\2\2"+
		"vw\3\2\2\2w(\3\2\2\2xy\7\f\2\2y*\3\2\2\2z|\t\5\2\2{z\3\2\2\2|}\3\2\2\2"+
		"}{\3\2\2\2}~\3\2\2\2~\177\3\2\2\2\177\u0080\b\26\2\2\u0080,\3\2\2\2\b"+
		"\2hmqv}\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}